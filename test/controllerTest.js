describe('Home Controller Test', function(){

  beforeEach(angular.mock.module('bridge-telecom'));

  beforeEach(angular.mock.inject(function($controller, $rootScope){

    mockScope = $rootScope.$new();

    controller = $controller('HomeCtrl', {
      $scope: mockScope
    });

  }));

  it('Проверка количества загруженных постов', function() {
    expect(mockScope.post.length).toEqual(2);
  });

});