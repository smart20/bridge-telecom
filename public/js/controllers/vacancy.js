exports = module.exports = ['$scope', 'Vacancy', function ($scope, Vacancy) {
  
  Vacancy.query().$promise.then(function(data){

    $scope.vacancies = data.map(function(arr){
      
      arr.duties = (arr.duties) ? arr.duties.replace(/p>/g, 'li>') : arr.duties;
      arr.requirements = (arr.requirements) ? arr.requirements.replace(/p>/g, 'li>') : arr.requirements;
      arr.conditions = (arr.conditions) ? arr.conditions.replace(/p>/g, 'li>') : arr.conditions;
      arr.additionally = (arr.additionally) ? arr.additionally.replace(/p>/g, 'li>') : arr.additionally;

      return arr;
    });
  });

}];