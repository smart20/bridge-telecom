var angular   = require('angular');
                require('angular-resource');
                require('angular-ui-router');
                require('angular-sanitize');
                require('angular-flickity');
                require('angular-animate');
                require('v-accordion');
                require('./vendor/multi-line-ellipsis');
                require('./vendor/ng-map.min');
angular
  .module('bridge-telecom', [
    'angularMultiLineEllipsis',
    'bc.Flickity',
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'vAccordion',
    'ngMap'
  ])

  .run(["$rootScope", "$anchorScroll", "$state", function($rootScope, $anchorScroll, $state){
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
      $anchorScroll();
    });
    $rootScope.$state = $state;
  }])

  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $stateProvider.
      state('home', {
        url: '/',
        templateUrl: 'templates/home',
        controller: 'homeCtrl'
      }).
      state('about', {
        url: '/about',
        templateUrl: 'templates/about'
      }).
      state('individual', {
        url: '/individual',
        templateUrl: 'templates/individual'
      }).
      state('rates', {
        url: '/individual/rates',
        templateUrl: 'templates/rates'
      }).
      state('vacancies', {
        url: '/about/vacancies',
        templateUrl: 'templates/vacancies',
        controller: 'vacancyCtrl'
      }).
      state('corporate', {
        url: '/corporate',
        templateUrl: 'templates/corporate'
      }).
      state('map', {
        url: '/map',
        templateUrl: 'templates/map',
        controller: 'mapCtrl'
      }).
      state('service', {
        url: '/service',
        templateUrl: 'templates/service',
        controller: 'serviceCtrl'
      }).
      state('contacts', {
        url: '/contacts',
        templateUrl: 'templates/contacts',
        controller: 'mapCtrl'
      }).
      state('request', {
        url: '/request',
        templateUrl: 'templates/request'
      }).
      state('support', {
        url: '/support',
        templateUrl: 'templates/support'
      }).
      state('accepted', {
        url: '/accepted',
        templateUrl: 'templates/accepted'
      }).      
      state('posts', {
        url: '/posts',
        templateUrl: 'templates/posts',
        controller: 'postsCtrl'
      }).
      state('post', {
        url: '/posts/:id',
        templateUrl: 'templates/post',
        controller: 'postCtrl'
      }).
      state('cabinet', {
        url: '/cabinet',
        templateUrl: 'templates/cabinet',
        controller: 'cabinetCtrl'
      });

      $locationProvider.html5Mode(true);
      $urlRouterProvider.otherwise('/');
  }])

  .controller('homeCtrl',        require('./controllers/home'))
  .controller('mapCtrl',         require('./controllers/map'))
  .controller('serviceCtrl',     require('./controllers/service'))
  .controller('cabinetCtrl',     require('./controllers/cabinet'))
  .controller('headerCtrl',      require('./controllers/header'))
  .controller('postsCtrl',       require('./controllers/posts'))
  .controller('postCtrl',        require('./controllers/post'))
  .controller('vacancyCtrl',     require('./controllers/vacancy'))
  .controller('flickityCtrl',    require('./controllers/flickity'))

  .factory('Home',               require('./services/home'))
  .factory('Post',               require('./services/post'))
  .factory('Map',                require('./services/map'))
  .factory('Vacancy',            require('./services/vacancy'))

  .filter('rawHtml', ['$sce', function($sce){
    return function(val) {
      return $sce.trustAsHtml(val);
    };
  }])
  .filter('htmlToPlaintext', function() {
    return function(text) {
      return angular.element(text).text();
    };
  })

  // .directive('loadDotdotdot', function(){
  //   return {
  //     restrict: 'A',
  //     link: function(scope, element, attributes) {
  //       scope.$watch(function() {
  //         element.dotdotdot({watch: true, wrap: 'letter'});
  //       });
  //     }
  //   };
  // })