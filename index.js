process.env.DEBUG = 'app';

var express     = require('express');
var path        = require('path');
var logger      = require('morgan');
var debug       = require('debug')('app');
var bodyParser  = require('body-parser');
// var through     = require('through2');
var enchilada   = require('enchilada');
var port        = process.env.PORT || 5000;
// var ploy        = require('./ploy-client');
var routes      = require('./routes');
var format      = require('util').format;
var sassMiddleware = require('node-sass-middleware');
var http        = require('http');

var app = express();

// var api         = ploy({
//   key:    '190bd73a440471c3150d0926a81ff941',
//   secret: 'f1b325a3d2b9139990ce23b7bcca4f08'
// });

var Mailgun     = require('mailgun-js');
var mailgun     = Mailgun({
  apiKey: 'key-25eb827f56213d9eff8a5df9053e407c',
  domain: 'sandboxaabd7df85cab447eab4c8e2983bcebaf.mailgun.org'
});

var production = process.env.NODE_ENV === 'production';
// console.log(production);
// app.use(logger('dev', {
//   stream: through(function(chunk, enc, cb) {
//     debug(chunk.toString().slice(0, chunk.length - 2));
//     cb();
//   })
// }));

app.use(enchilada({
  src: path.join(__dirname, 'public/js'),
  cache: production,
  compress: production
}));

// app.use(quesadilla(path.join(__dirname, '/css')));
app.use(sassMiddleware({
  src: path.join(__dirname, '/public/css'),
  dest: path.join(__dirname, '/public/css'),
  outputStyle: 'compressed',
  prefix:  '/css'
}));
app.use(express.static(__dirname + '/public',{maxage: 604800000}));
app.use('/fonts', express.static(__dirname + '/node_modules/font-awesome/fonts',{maxage: 31536000000}));

app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');

// var router = express.Router();

// router.get('/home.json', function(req, res, next) {
//   api.collection('posts').find({}, {'data.content': 0 }, function(err, posts) {

//     if (err) return next(err);

//     posts.sort(function(a, b){
//       var a_date = new Date(a.createdAt);
//       var b_date = new Date(b.createdAt);
//       if (a_date > b_date)
//         return -1;
//       else
//         return 1;
//     });

//     res.json(posts.slice(-2));
//   });
// });

// router.get('/posts.json', function(req, res, next) {
//   api.collection('posts').find({}, { 'data.content': 0 }, function(err, posts) {
//     if (err) return next(err);

//     posts.sort(function(a, b){
//       var a_date = new Date(a.createdAt);
//       var b_date = new Date(b.createdAt);
//       if (a_date > b_date)
//         return -1;
//       else
//         return 1;
//     });

//     res.json(posts);
//   });
// });

// router.get('/files.json', function(req, res, next) {
//   api.collection('files').find(function(err, files) {
//     if (err) return next(err);
//     res.json(files);
//   });
// });

// var cats = ['posts', 'vacancies'];

// router.get('/:category.json', function(req, res, next) {
//   var cat = req.params.category;
//   if (cats.indexOf(cat) < 0) return next();
//   api.collection(cat).find(function(err, pages) {
//     if (err) return next(err);
//     res.json(pages);
//   });
// });

// router.get('/:category/:id.json', function(req, res, next) {
//   var cat = req.params.category;
//   if (cats.indexOf(cat) < 0) return next();
//   api.collection(cat).find({ _id: req.params.id }, function(err, pages) {
//     if (err) {
//       console.log(err);
//       res.status(404).end();
//       return;
//     }
//     res.json(pages[0]);
//   });
// });

// app.use('/api', router);

// app.get('/files/*', function(req, res) {
//   res.type(mime.lookup(req.params[0]));
//   var stream = request('http://files.ploy.cc/' + req.params[0]);
//   stream.on('error', function(err) {
//     console.log('File download error', err);
//     res.status(404).end();
//   });
//   stream.pipe(res);
// });

app.post('/request',
  bodyParser.urlencoded({
    limit: '6mb',
    extended: false
  }),
  function(req, res) {

    // var post_data = querystring.stringify({
    //   'secret' : '6LfK8AYUAAAAAEk2RdNb77q4BjhaKod8y1HdW4mS',
    //   'response': req.body['g-recaptcha-response'],
    // });

    // var post_options = {
    //     host: 'www.google.com',
    //     port: '80',
    //     path: '/recaptcha/api/siteverify',
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/x-www-form-urlencoded',
    //         'Content-Length': Buffer.byteLength(post_data)
    //     }
    // };

    // var post_req = http.request(post_options, function(res) {
    //     res.setEncoding('utf8');
    //     res.on('data', function (chunk) {
    //         console.log('Response: ' + chunk);
    //     });
    // });

    // post_req.write({
    //   'secret' : '6LfK8AYUAAAAAEk2RdNb77q4BjhaKod8y1HdW4mS',
    //   'response': req.body['g-recaptcha-response'],
    // });
    // post_req.end();
    console.log(req.body);

    if (req.body['type'] == 'частного лица')
      var parts = [
        'Населенный пункт',
        'Адрес подключаемого объекта',
        'Контактное лицо',
        'Контактный телефон',
        'Email',
        'Поле для сообщения',
        'Реферальный номер'
      ];
    else
      var parts = [
        'Населенный пункт',
        'Адрес подключаемого объекта',
        'Контактное лицо',
        'Контактный телефон',
        'Email',
        'Поле для сообщения',
        'Количество рабочих мест(ИНТЕРНЕТ)',
        'Количество рабочих мест(VPN)',
        'Количество линий',
        'Количество номеров',
        'Красивый номер',
        'ВИДЕОНАБЛЮДЕНИЕ',
        'Wi-Fi',
        'СКС',
        'Реферальный номер'
      ];

    var data = {
      from: 'Сайт Бридж-Телеком <request@bridge-telecom.ru>',
      to: 'info@bridge-telecom.ru',
      // to: 'yakiwigo@gmail.com',
      subject: 'Заявка от '+req.body['type'],
      text: ''
    };

    parts.forEach(function(part) {
      data.text+= format('%s: %s\n', part.toUpperCase(), req.body[part], '\n');
    });

    mailgun.messages().send(data, function (error, body) {
      if (error)
        return res.redirect('/request');

      res.redirect('/accepted');
    });

  });

app.post('/support',
  bodyParser.urlencoded({
    limit: '6mb',
    extended: false
  }),
  function(req, res) {

    var parts = [
      'Населенный пункт',
      'Адрес подключаемого объекта',
      'Контактное лицо',
      'Номер лицевого счета',
      'Контактный телефон',
      'Email',
      'Поле для сообщения'
    ];

    var data = {
      from: 'Сайт Бридж-Телеком <support@bridge-telecom.ru>',
      to: 'info@bridge-telecom.ru',
      subject: 'Вопрос от '+req.body['type'],
      text: ''
    };

    parts.forEach(function(part) {
      data.text+= format('%s: %s\n', part.toUpperCase(), req.body[part], '\n');
    });
    
    mailgun.messages().send(data, function (error, body) {
      if (error)
        return res.redirect('/support');

      res.redirect('/accepted');
    });

  });

app.get('/', routes.index);
app.get('/templates/:name', routes.templates);
app.get('*', routes.index);

app.listen(port, function() {
  debug('Server is listening on port %s', port);
});
